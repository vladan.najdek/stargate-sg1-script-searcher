﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.XPath;

namespace StargateSG1ScriptSearcher
{
    class Episode
    {
        public string Season { get; set; }
        public string Number { get; set; }
        public string Name { get; set; }
        public string Summary { get; set; }
        public List<Line> Lines { get; set; } = new List<Line>();

        public Episode(string episodeWebpageText)
        {
            // Get episode details from header
            XDocument webpage = XDocument.Parse(episodeWebpageText); //1.01 "Children Of The Gods Part 1" Transcript - StargateWiki
            string details = webpage.Element("html").Element("head").Element("title").Value;

            Season = details.First().ToString();
            Number = details.Substring(2, 2);
            string helper = details.Substring(6);
            int index = helper.IndexOf("\"");
            Name = helper.Substring(0, index);

            XElement content = webpage.XPathSelectElement("//div[@class='mw-parser-output']");
            Summary = content.Element("p").Value;
            index = Summary.IndexOf("|");
            if (index >= 6) Summary = Summary.Substring(0, index - 6);

            // Get episode lines
            List<XElement> lines = content.Elements("blockquote").ToList();
            foreach (XElement line in lines)
            {
                string currentLine = line.ToString();
                currentLine = currentLine.Replace("<blockquote>", "").Replace("</blockquote>", "").Replace("<p>", "").Replace("</p>", "").Substring(4);
                index = currentLine.IndexOf('<');
                if (index != -1)
                {
                    string actor = currentLine.Substring(0, index);
                    string text = currentLine.Replace("<br />", "").Substring(index).Trim();
                    Lines.Add(new Line(actor, text));
                }
            }
        }
    }
}
