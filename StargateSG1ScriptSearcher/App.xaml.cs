﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace StargateSG1ScriptSearcher
{
    /// <summary>
    /// Interakční logika pro App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void ApplicationStart(object sender, StartupEventArgs e)
        {
            var mainVM = new MainViewmodel();
            var mainView = new MainWindow { DataContext = mainVM };
            mainView.Show();
        }
    }
}
