﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StargateSG1ScriptSearcher
{
    class SearchResult
    {
        public Line Line { get; set; }
        public Episode Episode { get; set; }

        public SearchResult(Line line, Episode episode)
        {
            Line = line;
            Episode = episode;
        }
    }
}
