﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace StargateSG1ScriptSearcher
{
    class MainViewmodel: INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged = (sender, e) => { };

        public void OnPropertyChanged(string name)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(name));
        }


        private Settings settings;
        private List<Episode> episodes = new List<Episode>();

        private float downloadProgress;
        public float DownloadProgress { get => downloadProgress; set { downloadProgress = value; PropertyChanged(this, new PropertyChangedEventArgs(nameof(DownloadProgress))); } }

        private bool donwloadButtonEnabled = true;
        public bool DonwloadButtonEnabled { get => donwloadButtonEnabled; set { donwloadButtonEnabled = value; PropertyChanged(this, new PropertyChangedEventArgs(nameof(DonwloadButtonEnabled))); } }

        public ICommand Search { get; set; }
        public ICommand Download { get; set; }

        public ObservableCollection<SearchResult> SearchResults { get; set; } = new ObservableCollection<SearchResult>();

        public string SearchedExpression
        {
            get
            {
                return settings.SearchedExpression;
            }
            set
            {
                settings.SearchedExpression = value;
            }
        }

        public MainViewmodel()
        {
            settings = new Settings();
            Search = new RelayCommand(SearchWiki);
            Download = new RelayCommand(DownloadFromWiki);
        }

        public void DownloadFromWiki()
        {
            DonwloadButtonEnabled = false;
            WebSeeker seeker = new WebSeeker(settings);
            seeker.ProgressUpdated += (progress) => { DownloadProgress = progress; };
            episodes.Clear();
            new Task(async () => { episodes = await seeker.SearchWeb(); DonwloadButtonEnabled = true; DownloadProgress = 0; }).Start();
            //episodes = seeker.SearchWeb();
        }

        public void SearchWiki()
        {
            SearchResults.Clear();

            // Search for expression from user input
            foreach (Episode ep in episodes)
            {
                foreach (Line line in ep.Lines)
                {
                    if (line.Text.ToLower().Contains(settings.SearchedExpression.ToLower()))
                    {
                        SearchResults.Add(new SearchResult(line, ep));
                    }
                }
            }
        }
    }
}
