﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Threading;
using System.Collections.Concurrent;

namespace StargateSG1ScriptSearcher
{
    class WebSeeker
    {
        private Settings settings;
        private Dictionary<int, string> numberWords = new Dictionary<int, string>()
        {
            { 1, "One" },
            { 2, "Two" },
            { 3, "Three" },
            { 4, "Four" },
            { 5, "Five" },
            { 6, "Six" },
            { 7, "Seven" },
            { 8, "Eight" },
            { 9, "Nine" },
            { 10, "Ten" },
        };

        public WebSeeker(Settings settings)
        {
            this.settings = settings;
        }

        public event Action<float> ProgressUpdated;

        public async Task<List<Episode>> SearchWeb()
        {
            List<Episode> episodes = new List<Episode>();
            ProgressUpdated(0.1f);

            for (int ii = 1; ii <= numberWords.Count; ii++) {
                // Download season page
                string page;
                using (var client = new WebClient() { Proxy = null})
                {
                    page = await client.DownloadStringTaskAsync("http://www.stargate-sg1-solutions.com/wiki/Season_" + numberWords[ii] + "_Transcripts");
                    string[] aHrefs = page.Split(new string[] { "a href=\"" }, StringSplitOptions.None);

                    // Filter out episode links
                    List<string> episodesLinks = new List<string>();
                    foreach (string s in aHrefs)
                    {
                        string currentString = s;
                        // Delete everything after "
                        int index = currentString.IndexOf("\"");
                        if (index > 0) currentString = currentString.Substring(0, index);

                        // Edit the string's special characters
                        currentString = currentString.Replace("%22", "\"").Replace("&#39;", "%27");

                        // Check link
                        if (!currentString.Contains("wiki") || currentString.Contains("File") || currentString.Contains("Episode_Guide") || episodesLinks.Contains(currentString)) continue;

                        // This string always appears after all episodes
                        if (currentString == "/wiki/Transcripts") break;

                        // Finally add
                        episodesLinks.Add(currentString);
                    }

                    // Create episode instances
                    var bagOfEpisodes = new ConcurrentBag<Episode>();
                    var tasks = episodesLinks.Select(async link =>
                    {
                        using (var pageClient = new WebClient() { Proxy = null })
                        {
                            page = await pageClient.DownloadStringTaskAsync("http://www.stargate-sg1-solutions.com/" + link);
                            bagOfEpisodes.Add(new Episode(page));
                        }
                    });
                    await Task.WhenAll(tasks);

                    episodes.AddRange(bagOfEpisodes);
                }
                ProgressUpdated(0.1f + 0.9f * ii / numberWords.Count);
            }
            
            return episodes;
        }
    }
}
