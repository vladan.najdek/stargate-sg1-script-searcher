﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StargateSG1ScriptSearcher
{
    class Line
    {
        public string Actor { get; set; }
        public string Text { get; set; }

        public Line(string actor, string text)
        {
            Actor = actor;
            Text = text;
        }
    }
}
